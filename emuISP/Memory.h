// Memory.h 2016/3/23
#pragma once
#include <stdint.h>
#include "mymap.h"

class Memory {
public:
    Memory(int flashSize = 0x4000, int sramSize = 0x1000, uint32_t sramBase = 0x10000000);
    bool Write(uint32_t addr, uint8_t c);
    uint8_t Read(uint32_t addr);
    bool Copy(uint32_t dst, uint32_t src, int count);

private:
    mymap<int,uint8_t*> flash;
    mymap<int,uint8_t*> sram;
    uint32_t flash_end;
    uint32_t sram_base;
    uint32_t sram_end;
};

