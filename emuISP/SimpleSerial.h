// SimpleSerial.h 2016/3/23
#include "mbed.h"

class SimpleSerial {
public:
    SimpleSerial(PinName tx, PinName rx):ser(tx,rx),rxbuf_w(0),rxbuf_r(0) {
        ser.baud(115200);
        ser.attach(this, &SimpleSerial::handler);
    }
    void baud(int baud_) { ser.baud(baud_); }
    void putc(int c) { ser.putc(c); }
    int getc() {
        if (!this->readable()) {
            return -1;
        }
        int c = rxbuf[rxbuf_r++];
        rxbuf_r &= sizeof(rxbuf)-1;
        return c;
    }
    bool readable() { return rxbuf_w != rxbuf_r; }

private:
    void handler() {
        while(ser.readable()) {
            rxbuf[rxbuf_w++] = ser.getc();
            rxbuf_w &= sizeof(rxbuf)-1;
        }
    }
    RawSerial ser;
    int rxbuf_w, rxbuf_r;
    char rxbuf[128];
};

