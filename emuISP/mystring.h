// mystring.h 2016/3/24
#pragma once
class mystring {
public:
    mystring() {
        m_cap = 4;
        m_buf = new char[m_cap];
        m_size = 0;
        m_buf[m_size] = '\0';
    }
    ~mystring() {
        delete[] m_buf;
    }
    const char *c_str() const {return m_buf; }
    void clear() {
        delete[] m_buf;
        m_cap = 4;
        m_buf = new char[m_cap];
        m_size = 0;
        m_buf[m_size] = '\0';
    }
    void operator +=(char c) { push_back(c); }
    bool operator ==(const char* s) const { return strcmp(m_buf, s) == 0; }
    void push_back(char c) {
        if (m_size+1 < m_cap) {
            m_cap += 4;
            char* new_buf = new char[m_cap];
            for(int i = 0; i < m_size; i++) {
                new_buf[i] = m_buf[i];
            }
            delete[] m_buf;
            m_buf = new_buf;
        }
        m_buf[m_size++] = c;
        m_buf[m_size] = '\0';
    }
    char& operator[](size_t pos) const { return at(pos); }
    char& at(size_t pos) const { return m_buf[pos]; }
    size_t size() const { return m_size; }

private:
    size_t m_size;
    size_t m_cap;
    char *m_buf;
};

