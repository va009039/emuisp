// BaseEmuISP.h 2016/3/25
#pragma once
#include <stdint.h>
#include "myvector.h"
#include "mystring.h"

typedef myvector<char*> vec_charPtr_t;
typedef myvector<int> vec_int_t;
typedef myvector<uint8_t> vec_byte_t;
typedef mystring string_t;

enum Mode_t {
    M_RESET = 0,
    M_SYNC,
    M_CMD,
    M_CMD_W_DATA,
    M_CMD_R_DATA,
    M_CMD_J,
    M_CMD_K,
    M_CMD_N,
    M_CMD_S,
};

enum ReturnCode_t {
    CMD_SUCCESS = 0,
    INVALID_COMMAND = 1,
    SRC_ADDR_ERROR = 2,
    DST_ADDR_ERROR = 3,
    SRC_ADDR_NOT_MAPPED = 4,
    DST_ADDR_NOT_MAPPED = 5,
    COUNT_ERROR = 6,
    INVALID_SECTOR = 7,
    SECTOR_NOT_BLANK = 8,
    SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION = 9,
    COMPARE_ERROR = 10,
    BUSY = 11,
    PARAM_ERROR = 12,
    ADDR_ERROR = 13,
    ADDR_NOT_MAPPED = 14,
    CMD_LOCKED = 15,
    INVALID_CODE = 16,
    INVALID_BAUD_RATE = 17,
    INVALID_STOP_BIT = 18,
    CODE_READ_PROTECTIN_ENABLED = 19,
};

class BaseEmuISP {
public:
    void Reset();
    void Poll();

protected:
    virtual int BootCodeVersion() { return 4<<8|13; } // 4.13
    virtual int PartID() { return 0x00008100; } // LPC810M021FN8
    virtual bool UuencodeMode() { return false; }
    virtual bool WriteData(int addr, int c) { return false; }
    virtual int ReadData(int addr) { return 0; }
    virtual bool CopyData(int dst, int src, int count) { return false; }
    virtual bool Compare(int addr1, int addr2, int count) { return true; }
    virtual bool Prepare(int sector) { return true; }
    virtual bool Erase(int sector) { return true; }
    virtual bool Blank(int sector) { return true; }
    virtual int Getch() = 0;
    virtual void Putch(int c) = 0;
    virtual void DebugPutch(int c) { return; }

private:
    bool sync();
    ReturnCode_t cmd(const char* s);
    ReturnCode_t cmd_u(vec_int_t &param);
    ReturnCode_t cmd_b(vec_int_t &param);
    ReturnCode_t cmd_a(vec_int_t &param);
    ReturnCode_t cmd_w(vec_int_t &param);
    ReturnCode_t cmd_r(vec_int_t &param);
    ReturnCode_t cmd_p(vec_int_t &param);
    ReturnCode_t cmd_c(vec_int_t &param);
    ReturnCode_t cmd_g(vec_int_t &param);
    ReturnCode_t cmd_e(vec_int_t &param);
    ReturnCode_t cmd_i(vec_int_t &param);
    ReturnCode_t cmd_j(vec_int_t &param);
    ReturnCode_t cmd_k(vec_int_t &param);
    ReturnCode_t cmd_m(vec_int_t &param);
    ReturnCode_t cmd_n(vec_int_t &param);
    ReturnCode_t cmd_s(vec_int_t &param);
    bool cmd_w_data();
    bool cmd_r_data();
    void putln(const char *s);
    void debugPrintf(const char *format, ...);
    Mode_t mode;
    int seq;
    bool echoFlag;
    bool lockFlag;
    string_t line;
    bool lineProc();
    int freq;
    int addr;
    struct {
        vec_byte_t Buf;
        int Line;
        int Count;
        int Current;
        int Cksum;
    } data;
    struct {
        uint8_t Major;
        uint8_t Minor;
     } version;
};

