//　main.cpp 2016/3/23
// UART ISP emulator
#include "mbed.h"
#include "SimpleSerial.h"
#include "Memory.h"
#include "BaseEmuISP.h"

#if defined(TARGET_LPC1768)
SimpleSerial uart(USBTX, USBRX);
RawSerial dbg(p9, p10);

#elif defined(TARGET_NUCLEO_L152RE)
SimpleSerial uart(USBTX, USBRX);
RawSerial dbg(PB_10, PB_11);

#else
#error "target error"
#endif

#ifndef LPC1114FN28_ISP
#define LPC810FN8_ISP
#endif

#if defined(LPC810FN8_ISP)
class emuISP : public BaseEmuISP {
public:
    emuISP():mem(0x4000, 0x1000) {}
    virtual int BootCodeVersion() { return 4<<8|13; } // 4.13
    virtual int PartID() { return 0x00008100; } // LPC810M021FN8
    virtual int Getch() {return uart.readable() ? uart.getc() : -1;}
    virtual void Putch(int c) {uart.putc(c);}
    virtual void DebugPutch(int c) {dbg.putc(c);}
    virtual bool WriteData(int addr, int c) { return mem.Write(addr, c);}
    virtual int ReadData(int addr) {return mem.Read(addr);}
    virtual bool CopyData(int dst, int src, int count) { return mem.Copy(dst, src, count);}
    Memory mem;
} isp;

#elif defined(LPC1114FN28_ISP)
class emuISP : public BaseEmuISP {
public:
    emuISP():mem(0x8000, 0x4000) {}
    virtual int BootCodeVersion() { return 1<<8|7; } // 1.7
    virtual int PartID() { return 0x1a40902b; } // LPC1114FN28/102
    virtual bool UuencodeMode() { return true; }
    virtual int Getch() {return uart.readable() ? uart.getc() : -1;}
    virtual void Putch(int c) {uart.putc(c);}
    virtual void DebugPutch(int c) {dbg.putc(c);}
    virtual bool WriteData(int addr, int c) { return mem.Write(addr, c);}
    virtual int ReadData(int addr) {return mem.Read(addr);}
    virtual bool CopyData(int dst, int src, int count) { return mem.Copy(dst, src, count);}
    Memory mem;
} isp;
#endif


int main() {
    uart.baud(9600);
    dbg.baud(115200);

    isp.Reset();
    for(;;) {
        isp.Poll();
    }
}

