// Memory.cpp 2016/3/23
#include "mbed.h"
#include "Memory.h"

static const uint32_t memChunk = 1024;

Memory::Memory(int flashSize, int sramSize, uint32_t sramBase) {
    flash_end = flashSize;
    sram_base = sramBase;
    sram_end = sram_base + sramSize;
}

bool Memory::Write(uint32_t addr, uint8_t c) {
    if (addr < flash_end) {
        int sector = addr / memChunk;
        if (flash[sector] == NULL) {
            flash[sector] = (uint8_t*)malloc(memChunk);
            if (flash[sector] == NULL) {
                return false;
            }
        }
        flash[sector][addr%memChunk] = c;
    } else if (addr >= sram_base && addr < sram_end) {
        int sector = (addr - sram_base) / memChunk;
        if (sram[sector] == NULL) {
            sram[sector] = (uint8_t*)malloc(memChunk);
            if (sram[sector] == NULL) {
                return false;
            }
        }
        sram[sector][addr%memChunk] = c;
    }
    return true;
}

uint8_t Memory::Read(uint32_t addr) {
    if (addr < flash_end) {
        int sector = addr / memChunk;
        if (flash[sector] == NULL) {
            return 0xff;
        }
        return flash[sector][addr%memChunk];
    } else if (addr >= sram_base && addr < sram_end) {
        int sector = (addr - sram_base) / memChunk;
        if (sram[sector] == NULL) {
            return 0x00;
        }
        return sram[sector][addr%memChunk];
    }
    return 0x00;
}

bool Memory::Copy(uint32_t dst, uint32_t src, int count) {
    bool ok = true;
    while(count-- > 0 && ok) {
        ok = Write(dst++, Read(src++));
    }
    return ok;
}

